import Axios from 'axios';
import { store as notificationStore } from 'react-notifications-component';

import {
  SET_DISPLAY_RANGE,
  FETCH_METEORITE_DATA,
  API_ENDPOINT
} from '../constants';

export function setDisplayRange(payload) {
  return { type: SET_DISPLAY_RANGE, payload };
}

export const fetchMeteorites = meteorites => {
  return {
    type: FETCH_METEORITE_DATA,
    meteorites
  };
};

/**
 * fetch meteorite data from api
 */
export const fetchAllMeteorites = () => {
  return dispatch => {
    return Axios.get(API_ENDPOINT.base + API_ENDPOINT.meteorites)
      .then(res => {
        notificationStore.addNotification({
          title: 'Success',
          message: 'Data loaded successfully!',
          type: 'success',
          insert: 'top',
          container: 'top-right',
          animationIn: ['animated', 'fadeIn'],
          animationOut: ['animated', 'fadeOut'],
          dismiss: {
            duration: 5000,
            onScreen: true
          }
        });
        const meteorites = res.data;
        dispatch(fetchMeteorites(meteorites));
      })
      .catch(error => {
        notificationStore.addNotification({
          title: 'Failure',
          message: 'Failed to load data!',
          type: 'danger',
          insert: 'top',
          container: 'top-right',
          animationIn: ['animated', 'fadeIn'],
          animationOut: ['animated', 'fadeOut'],
          dismiss: {
            duration: 5000,
            onScreen: true
          }
        });
        throw error;
      });
  };
};
