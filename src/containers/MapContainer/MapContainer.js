import React, { Component } from 'react';
import { connect } from 'react-redux';
import MeteoriteMap from '../../components/MeteoriteMap/MeteoriteMap';
import { fetchAllMeteorites } from '../../actions';

const mapStateToProps = state => {
  const { meteorites } = state;
  return { meteorites };
};
const mapDispatchToProps = dispatch => ({
  doFetchAllMeteorites: () => dispatch(fetchAllMeteorites())
});

export class MapContainer extends Component {
  componentDidMount() {
    // const { doFetchAllMeteorites } = this.props;
    //  doFetchAllMeteorites();
  }

  render() {
    const { meteorites = [] } = this.props;
    return (
      <div>
        <MeteoriteMap meteorites={meteorites} />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MapContainer);
