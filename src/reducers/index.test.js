import rootReducer, { initialState } from './index';
import {
  SET_DISPLAY_RANGE,
  FETCH_METEORITE_DATA,
  MOCK_DATA
} from '../constants';

describe('rootReducer', () => {
  test('should return the initial state', () => {
    expect(rootReducer(undefined, {})).toEqual(initialState);
  });

  test('should handle SET_DISPLAY_RANGE', () => {
    const startAction = {
      type: SET_DISPLAY_RANGE,
      payload: {
        start: '2010-01-01',
        end: '2011-01-01'
      }
    };
    expect(rootReducer({}, startAction)).toEqual({
      displayRange: {
        end: '2011-01-01',
        start: '2010-01-01'
      }
    });
  });
  test('should handle FETCH_METEORITE_DATA', () => {
    const startAction = {
      type: FETCH_METEORITE_DATA,
      meteorites: MOCK_DATA
    };
    expect(rootReducer(undefined, startAction)).toEqual({
      ...initialState,
      meteorites: MOCK_DATA
    });
  });
});
