import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import ReactNotification from 'react-notifications-component';
import { Sticky } from 'semantic-ui-react';
import 'react-notifications-component/dist/theme.css';
import './App.scss';
import { Header } from '../Header';
import MapPage from '../MapPage/MapPage';

const RouteNotAvailable = ({ location }) => (
  <div className="App__page App__page--not-available">
    <span>
      Invalid path: <em>{location.pathname}</em>, this page does not exist.
    </span>
  </div>
);

function App() {
  return (
    <div className="App">
      <ReactNotification />
      <Router>
        <Sticky>
          <Header />
        </Sticky>
        <Switch>
          <Route exact path="/" render={() => <Redirect to="/map" />} />
          <Route path="/map" exact component={MapPage} />
          <Route component={RouteNotAvailable} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
