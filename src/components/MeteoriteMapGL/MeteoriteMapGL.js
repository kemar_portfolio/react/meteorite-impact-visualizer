import React, { Component } from 'react';
import MapGL, {
  Popup,
  NavigationControl,
  FullscreenControl,
  ScaleControl
} from 'react-map-gl';
import { MAPBOX_TOKEN } from '../../constants';
import MeteoritePin from '../MeteoritePin/MeteoritePin';
import './MeteoriteMapGL.scss';
import MeteoriteInfoPopup from '../MeteoriteInfoPopup/MeteoriteInfoPopup';

export default class MeteoriteMapGL extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewport: {
        latitude: 37.785164,
        longitude: -100,
        zoom: 0,
        bearing: 0,
        pitch: 0
      },
      popupInfo: null
    };
    this.onClickMarker = this.onClickMarker.bind(this);
  }

  _updateViewport = viewport => {
    this.setState({ viewport });
  };

  /**
   * Show popup on marker click
   */
  onClickMarker = meteorite => {
    this.setState({ popupInfo: meteorite });
  };

  renderPopup() {
    const { popupInfo } = this.state;

    return (
      popupInfo && (
        <Popup
          tipSize={5}
          anchor="top"
          longitude={parseFloat(popupInfo.recLong)}
          latitude={parseFloat(popupInfo.recLat)}
          closeOnClick={false}
          onClose={() => this.setState({ popupInfo: null })}
        >
          <MeteoriteInfoPopup info={popupInfo} />
        </Popup>
      )
    );
  }

  render() {
    const { viewport } = this.state;
    const { meteoriteImpacts } = this.props;

    return (
      <MapGL
        {...viewport}
        width="100%"
        height="100%"
        mapStyle="mapbox://styles/mapbox/dark-v9"
        onViewportChange={this._updateViewport}
        mapboxApiAccessToken={MAPBOX_TOKEN}
        className="MeteoriteMapGL"
      >
        <MeteoritePin data={meteoriteImpacts} onClick={this.onClickMarker} />
        {this.renderPopup()}
        <div className="MeteoriteMapGL__full-screen">
          <FullscreenControl />
        </div>
        <div className="MeteoriteMapGL__nav-control">
          <NavigationControl />
        </div>
        <div className="MeteoriteMapGL__scale-screen">
          <ScaleControl />
        </div>
      </MapGL>
    );
  }
}
