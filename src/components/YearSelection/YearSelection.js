import React, { Component } from 'react';
import xor from 'lodash/xor';
import { Button } from 'semantic-ui-react';
import './YearSelection.scss';

export default class YearSelection extends Component {
  yearCt = 30;

  constructor(props) {
    super(props);

    this.state = {
      decade: 2010,
      selected: [],
      highlighted: []
    };
    this.onSelectionChange = this.onSelectionChange.bind(this);
    this.notifySelection = this.notifySelection.bind(this);
    this.nextDecade = this.nextDecade.bind(this);
    this.prvDecade = this.prvDecade.bind(this);
  }

  onSelectionChange(year) {
    const { selected } = this.state;
    const isSelected = selected.includes(year);
    const { condition } = this.props;
    let selectedUpdate = [];
    let highlightedUpdate = [];

    if (condition === 'eq' || condition === 'neq') {
      if (!isSelected) {
        selectedUpdate = [year];
        highlightedUpdate = [year];
      } else {
        selectedUpdate = [];
        highlightedUpdate = [];
      }
    } else if (condition === 'in' || condition === 'n_in') {
      selectedUpdate = xor(selected, [year]);
      highlightedUpdate = xor(selected, [year]);
    } else if (condition === 'lte') {
      selectedUpdate = [year];
      highlightedUpdate = this.years().filter(y => y <= year);
    } else if (condition === 'gte') {
      selectedUpdate = [year];
      highlightedUpdate = this.years().filter(y => y >= year);
    } else if (condition === 'range' || condition === 'n_range') {
      const [start, end] = selected;
      if ((!start && !end) || (start && end)) {
        selectedUpdate = [year];
        highlightedUpdate = [year];
      } else if (start && !end) {
        selectedUpdate = [start, year];
        highlightedUpdate = this.years().filter(y => y >= start && y <= year);
      }
    }
    this.setState(
      {
        selected: selectedUpdate,
        highlighted: highlightedUpdate
      },
      () => {
        this.notifySelection();
      }
    );
  }

  years = y => {
    const currentYear = new Date().getFullYear();
    const years = [];
    let startYear = y || 1400;
    while (startYear < currentYear) {
      years.push((startYear += 1));
    }
    return years.sort();
  };

  notifySelection() {
    const { onChange, id } = this.props;
    const { selected } = this.state;
    onChange(null, { id, value: selected });
  }

  handleKeyPress() {}

  prvDecade() {
    const { decade } = this.state;
    this.setState({ decade: decade - 10 });
  }

  nextDecade() {
    const { decade } = this.state;
    this.setState({ decade: decade + 10 });
  }

  render() {
    const { highlighted, decade } = this.state;
    const isNextDecadeEnabled = () => new Date().getFullYear() - decade < 10;
    return (
      <div className="YearSelection">
        <div className="YearSelection__decade-toggle">
          <Button icon="left chevron" onClick={this.prvDecade} />
          <span className="decade">{decade}</span>
          <Button
            icon="right chevron"
            onClick={this.nextDecade}
            disabled={isNextDecadeEnabled()}
          />
        </div>
        <div className="YearSelection__blocks">
          {this.years()
            .filter(year => year >= decade && year <= decade + 9)
            .map(year => {
              const isHighlighted = highlighted.includes(year);
              return (
                <div
                  className={`YearSelection__block ${isHighlighted &&
                    'highlighted'}`}
                  onClick={() => this.onSelectionChange(year)}
                  role="button"
                  tabIndex={0}
                  onKeyPress={this.handleKeyPress}
                  key={year}
                >
                  <span>{year}</span>
                </div>
              );
            })}
        </div>
      </div>
    );
  }
}
