import React from 'react';
import { Icon } from 'semantic-ui-react';
import './FilterPill.scss';
import { conditionSymbols } from '../../constants';

export default function FilterPill(props) {
  const { filter, removePill, editPill } = props;
  const { condition, value, field, id } = filter;

  return (
    <div className="FilterPill">
      <div className="FilterPill__top">
        <Icon name="remove circle" onClick={() => removePill(id)} />
        <Icon name="edit" onClick={() => editPill(id)} />
      </div>
      <div className="FilterPill__bottom">
        <div className="FilterPill__bottom__field">
          <span>{field}</span>
        </div>
        <div className="FilterPill__bottom__condition">
          <span>{conditionSymbols.get(condition)}</span>
        </div>
        <div className="FilterPill__bottom__value">
          {condition !== 'range' ? (
            <span>{value}</span>
          ) : (
            <span>
              {value[0]} - {value[1]}
            </span>
          )}
        </div>
      </div>
    </div>
  );
}
