import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import MapPage from './MapPage';

Enzyme.configure({ adapter: new Adapter() });

describe('MapPage.js', () => {
  const wrapper = shallow(<MapPage />);

  test('should render', () => {
    expect(wrapper.exists()).toBe(true);
  });
});
