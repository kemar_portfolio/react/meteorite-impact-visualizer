import React from 'react';
import { Button, Icon } from 'semantic-ui-react';

import MapContainerComp from '../../containers/MapContainer/MapContainer';
import './MapPage.scss';

export default function MapPage() {
  return (
    <div className="App__page MapPage">
      <div className="MapPage__header">
        <span className="App__page__title">Impact Map</span>
        <Button icon labelPosition="right">
          Add Map
          <Icon name="plus" />
        </Button>
      </div>
      <div className="MapPage__maps">
        <MapContainerComp />
      </div>
    </div>
  );
}
