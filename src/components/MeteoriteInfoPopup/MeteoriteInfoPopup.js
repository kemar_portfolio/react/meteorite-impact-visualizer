import React from 'react';
import './MeteoriteInfoPopup.scss';

export default function MeteoriteInfoPopup(props) {
  const { info } = props;
  const {
    name,
    recClass,
    mass,
    fall,
    year,
    nameType,
    recLat,
    recLong,
    id
  } = info;
  return (
    <div className="MeteoriteInfoPopup">
      <div className="MeteoriteInfoPopup__header">{name}</div>
      <div className="MeteoriteInfoPopup__body">
        <div className="info-row">
          <span>id: </span>
          <span>{id}</span>
        </div>
        <div className="info-row">
          <span>Rec class: </span>
          <span>{recClass}</span>
        </div>
        <div className="info-row">
          <span>Name type: </span>
          <span>{nameType}</span>
        </div>
        <div className="info-row">
          <span>Mass: </span>
          <span>{mass}</span>
        </div>
        <div className="info-row">
          <span>Fall/Found: </span>
          <span>{fall}</span>
        </div>
        <div className="info-row">
          <span>Year: </span>
          <span>{year}</span>
        </div>
        <div className="info-row">
          <span>Longitude: </span>
          <span>{recLong}</span>
        </div>
        <div className="info-row">
          <span>Latitude: </span>
          <span>{recLat}</span>
        </div>
      </div>
    </div>
  );
}
