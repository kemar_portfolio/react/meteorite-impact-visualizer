import React, { Component } from 'react';
import { Button, Icon } from 'semantic-ui-react';

import './QueryBar.scss';
import FilterPill from '../FilterPill/FilterPill';

export default class MapQueryBar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      openFilterPanel,
      filters,
      clearFilters,
      removePill,
      editPill
    } = this.props;
    return (
      <div className="QueryBar">
        <div className="QueryBar__pills">
          {filters.map((filter, i) => (
            <FilterPill
              key={i}
              filter={filter}
              removePill={removePill}
              editPill={editPill}
            />
          ))}
        </div>
        <Button.Group icon>
          <Button>
            <Icon name="refresh" />
          </Button>
          <Button onClick={clearFilters}>
            <Icon name="delete" />
          </Button>
          <Button onClick={openFilterPanel}>
            <Icon name="add" />
          </Button>
        </Button.Group>
      </div>
    );
  }
}
