import React, { Component } from 'react';
import { Statistic } from 'semantic-ui-react';
import axios from 'axios';
import remove from 'lodash/remove';
import find from 'lodash/find';
import findIndex from 'lodash/findIndex';
import './MeteoriteMap.scss';
import QueryBar from '../QueryBar/QueryBar';
import MeteoriteMapGL from '../MeteoriteMapGL/MeteoriteMapGL';
import FilterBuilder from '../FilterBuilder/FilterBuilder';
import { API_ENDPOINT } from '../../constants';

const Loader = require('react-loader');

export default class MeteoriteMap extends Component {
  constructor(props) {
    super(props);
    const { meteorites } = props;
    this.state = {
      isFilterEditorVisible: false,
      filters: [],
      meteorites,
      count: meteorites.length,
      loaded: true,
      editingFilter: null
    };
    this.onOpenFilterPanel = this.onOpenFilterPanel.bind(this);
    this.onCloseFilterPanel = this.onCloseFilterPanel.bind(this);
    this.onFilterSave = this.onFilterSave.bind(this);
    this.onClearFilters = this.onClearFilters.bind(this);
    this.onRemovePill = this.onRemovePill.bind(this);
    this.onEditPill = this.onEditPill.bind(this);
  }

  componentDidMount() {
    this.fetchMeteoritesByQuery();
  }

  onOpenFilterPanel() {
    this.setState({ isFilterEditorVisible: true });
  }

  onCloseFilterPanel() {
    this.setState({ isFilterEditorVisible: false });
  }

  onFilterSave(filter) {
    const { filters } = this.state;
    const { id } = filter;
    const exitingFilterIndex = findIndex(filters, { id });

    if (exitingFilterIndex !== -1) {
      filters[exitingFilterIndex] = { ...filter };
    } else {
      filters.push(filter);
    }

    this.setState({ filters, isFilterEditorVisible: false }, () => {
      this.fetchMeteoritesByQuery();
    });
  }

  onClearFilters() {
    this.setState({ filters: [] }, () => {
      this.fetchMeteoritesByQuery();
    });
  }

  onRemovePill(id) {
    const { filters } = this.state;
    remove(filters, f => f.id === id);
    this.setState({ filters });
    this.fetchMeteoritesByQuery();
  }

  onEditPill(id) {
    const { filters } = this.state;
    const editingFilter = find(filters, ['id', id]);
    this.setState({ editingFilter, isFilterEditorVisible: true });
  }

  fetchMeteoritesByQuery() {
    const { filters } = this.state;
    const options = {
      url: API_ENDPOINT.base + API_ENDPOINT.meteoritesQuery,
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json;charset=UTF-8'
      },
      data: filters
    };
    this.setState({ loaded: false });

    axios(options).then(response => {
      const {
        data: { meteorites, count }
      } = response;
      this.setState({
        meteorites,
        count,
        loaded: true
      });
    });
  }

  render() {
    const {
      isFilterEditorVisible,
      filters,
      meteorites,
      count,
      loaded,
      editingFilter
    } = this.state;
    return (
      <div className="MeteoriteMap">
        <div className="MeteoriteMap__header">
          <div className="MeteoriteMap__header__title">
            <Statistic color="orange" size="tiny" inverted>
              <Statistic.Value>{count}</Statistic.Value>
              <Statistic.Label>Meteorites</Statistic.Label>
            </Statistic>
          </div>
          <QueryBar
            className="MeteoriteMap__header__queryBar"
            clearFilters={this.onClearFilters}
            openFilterPanel={this.onOpenFilterPanel}
            filters={filters}
            removePill={this.onRemovePill}
            editPill={this.onEditPill}
          />
        </div>
        <div className="MeteoriteMap__body">
          <Loader loaded={loaded} options={{ color: '#fff' }}>
            <MeteoriteMapGL
              className="MeteoriteMap__body__map"
              meteoriteImpacts={meteorites}
            />
          </Loader>
          <FilterBuilder
            className="MeteoriteMap__body__filter-editor"
            isVisible={isFilterEditorVisible}
            closeFilterPanel={this.onCloseFilterPanel}
            filterSave={this.onFilterSave}
            filter={editingFilter}
          />
        </div>
      </div>
    );
  }
}
