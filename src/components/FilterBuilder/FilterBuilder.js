/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import uuid from 'react-uuid';
import { Button, Form, Icon } from 'semantic-ui-react';
import NumberInput from 'semantic-ui-react-numberinput';

import YearSelection from '../YearSelection/YearSelection';
import './FilterBuilder.scss';
import {
  FILTER_CONDITIONS,
  FILTER_FIELDS,
  FALL_OPTIONS
} from '../../constants';

export default class FilterBuilder extends React.Component {
  constructor(props) {
    super(props);
    const { isVisible } = props;
    this.state = {
      condition: null,
      field: null,
      value: '',
      isVisible
    };
    this.handleValueChange = this.handleValueChange.bind(this);
    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleConditionChange = this.handleConditionChange.bind(this);
    this.onAddMassInput = this.onAddMassInput.bind(this);
    this.onRemoveMassInput = this.onRemoveMassInput.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.handleSaveFilter = this.handleSaveFilter.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.isVisible === false && nextProps.isVisible === true) {
      const { filter, isVisible } = nextProps;
      if (filter) {
        const { field, condition, value } = filter;

        return {
          isVisible,
          condition,
          field,
          value
        };
      }

      return {
        isVisible,
        condition: null,
        field: null,
        value: ''
      };
    }
    return {
      isVisible: nextProps.isVisible
    };
  }

  onCancel() {
    const { closeFilterPanel } = this.props;
    this.setState({ field: null, condition: null, value: null }, () => {
      closeFilterPanel();
    });
  }

  onAddMassInput() {
    const { value, field } = this.state;
    if (field === 'mass') {
      value.push({ itemVal: '100' });
    } else {
      value.push({ itemVal: '0' });
    }
    this.setState({ value });
  }

  onRemoveMassInput(index) {
    const { value } = this.state;
    if (value.length === 1) return;
    this.setState({ value: value.filter((n, i) => i !== index) });
  }

  getValidFilterConditions() {
    const { field } = this.state;

    switch (field) {
      case 'fall':
        return FILTER_CONDITIONS.filter(c => ['eq', 'neq'].includes(c.key));
      case 'name':
        return FILTER_CONDITIONS.filter(c =>
          ['eq', 'neq', 'in', 'n_in', 'sw', 'ew', 'contains'].includes(c.key)
        );
      case 'mass':
        return FILTER_CONDITIONS.filter(c =>
          ['eq', 'neq', 'in', 'n_in', 'lte', 'gte'].includes(c.key)
        );
      case 'year':
        return FILTER_CONDITIONS.filter(c =>
          [
            'eq',
            'neq',
            'in',
            'n_in',
            'lte',
            'gte',
            'range',
            'n_range'
          ].includes(c.key)
        );
      default:
        return FILTER_CONDITIONS;
    }
  }

  handleFieldChange = (e, { id, value }) => {
    if (value === 'mass') {
      this.setState({ value: '100' });
    } else if (value === 'recLat' || value === 'recLong') {
      this.setState({ value: '0' });
    } else {
      this.setState({ value: '' });
    }
    this.setState({ [id]: value });
  };

  handleConditionChange = (e, { id, value }) => {
    const { field } = this.state;

    if (field === 'recLat' || field === 'recLong') {
      if (value === 'in' || value === 'n_in') {
        this.setState({ value: [{ itemVal: '0' }] });
      } else {
        this.setState({ value: '0' });
      }
    } else if (field === 'mass') {
      if (value === 'in' || value === 'n_in') {
        this.setState({ value: [{ itemVal: '100' }] });
      } else {
        this.setState({ value: '100' });
      }
    }
    this.setState({ [id]: value });
  };

  handleValueChange = (e, { id, value, index }) => {
    const { field, condition, value: stateVal } = this.state;
    if (
      (field === 'mass' || field === 'recLat' || field === 'recLong') &&
      (condition === 'in' || condition === 'n_in')
    ) {
      stateVal[index].itemVal = value;
      this.setState({ value: stateVal });
    } else {
      this.setState({ value });
    }
  };

  valueElement() {
    const { field, condition, value } = this.state;
    if (field === null || condition === null) return '';

    switch (field) {
      case 'mass':
      case 'recLat':
      case 'recLong': {
        let numInputConfig = {
          stepAmount: 100,
          minValue: 0,
          size: 'small'
        };

        if (field === 'recLat') {
          numInputConfig = {
            ...numInputConfig,
            minValue: -90,
            maxValue: 90,
            stepAmount: 1
          };
        } else if (field === 'recLong') {
          numInputConfig = {
            ...numInputConfig,
            minValue: -90,
            maxValue: 90,
            stepAmount: 1
          };
        }

        if (condition === 'in' || condition === 'n_in') {
          if (!Array.isArray(value)) return '';
          return (
            <div className="number-input-group">
              <div className="number-input-group__header">
                <span className="field-label">Value: </span>
                <Icon.Group size="large" onClick={this.onAddMassInput}>
                  <Icon name="weight" inverted color="green" />
                  <Icon corner name="add" inverted color="black" />
                </Icon.Group>
              </div>
              {value.map((val, i) => (
                <div className="number-input-group__row" key={uuid()}>
                  <NumberInput
                    value={val.itemVal}
                    {...numInputConfig}
                    onChange={v =>
                      this.handleValueChange(null, {
                        id: 'value',
                        value: v,
                        index: i
                      })
                    }
                  />
                  <Icon
                    size="large"
                    name="minus"
                    inverted
                    color="red"
                    onClick={() => this.onRemoveMassInput(i)}
                  />
                </div>
              ))}
            </div>
          );
        }
        return (
          // eslint-disable-next-line react/jsx-fragments
          <React.Fragment>
            <span className="field-label">Value: </span>
            <NumberInput
              {...numInputConfig}
              value={value}
              label="Value:"
              onChange={v =>
                this.handleValueChange(null, { id: 'value', value: v })
              }
            />
          </React.Fragment>
        );
      }
      case 'fall':
        return (
          <Form.Select
            fluid
            label="Value:"
            id="value"
            value={value}
            options={FALL_OPTIONS}
            placeholder="Fall or Found"
            onChange={this.handleValueChange}
          />
        );
      case 'year':
        return (
          <YearSelection
            id="year"
            onChange={this.handleValueChange}
            condition={condition}
          />
        );
      default:
        return (
          <Form.Input
            id="value"
            fluid
            label="Value:"
            placeholder={field}
            onChange={this.handleValueChange}
          />
        );
    }
  }

  handleSaveFilter() {
    const { filterSave, filter: filterIn } = this.props;
    const { field, condition, value: stateVal } = this.state;
    let value = stateVal;
    if (field === 'mass' || field === 'recLat' || field === 'recLong') {
      value = Array.isArray(value)
        ? value.map(v => v.itemVal).join(' , ')
        : value;
    } else if (field === 'year') {
      if (condition === 'range' || condition === 'n_range') {
      } else {
        value = value.join(', ');
      }
    }

    const filter = {
      id: filterIn ? filterIn.id : uuid(),
      field,
      condition,
      value
    };

    filterSave(filter);
  }

  render() {
    const { isVisible, filter } = this.props;
    const { field } = this.state;

    return (
      <div className={`FilterBuilder ${isVisible ? '' : 'hidden'}`}>
        <div className="FilterBuilder__header">
          <span className="FilterBuilder__header__title">Add Filter</span>
        </div>
        <div className="FilterBuilder__body">
          <div className="FilterBuilder__body__fields">
            {isVisible && (
              <Form.Select
                fluid
                label="Field:"
                id="field"
                value={field}
                options={FILTER_FIELDS}
                placeholder="Select field"
                onChange={this.handleFieldChange}
              />
            )}

            {field && (
              <Form.Select
                fluid
                label="Condition:"
                id="condition"
                options={this.getValidFilterConditions()}
                placeholder="Select condition"
                onChange={this.handleConditionChange}
              />
            )}
            <div className="field">{this.valueElement()}</div>
          </div>
          <div className="FilterBuilder__body__actions">
            <Button.Group>
              <Button onClick={this.onCancel}>Cancel</Button>
              <Button.Or />
              <Button positive onClick={this.handleSaveFilter}>
                {filter ? 'Update' : 'Add'}
              </Button>
            </Button.Group>
          </div>
        </div>
      </div>
    );
  }
}
