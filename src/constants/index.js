export * from './actionTypes';
export * from './mocks';
export const MAPBOX_TOKEN =
  'pk.eyJ1Ijoia2VtYXJiZWxsIiwiYSI6ImNrNnd2M2ZpYzA2Y3EzbG95NzRzMmtyYTUifQ.iXMHPxIffEPmQE1UWxeV_w';

export const API_ENDPOINT = {
  nasa: 'https://data.nasa.gov/resource/y77d-th95.json',
  base: 'http://localhost:3000',
  meteorites: '/api/meteorites/getAll',
  meteoritesQuery: '/api/meteorites/get'
};

export const REC_CLASSES = [
  'L5',
  'H6',
  'EH4',
  'Acapulcoite',
  'L6',
  'LL3-6',
  'H5',
  'L',
  'Diogenite-pm',
  'Unknown',
  'H4',
  'H',
  'Iron, IVA',
  'CR2-an',
  'LL5',
  'CI1',
  'L/LL4',
  'Eucrite-mmict',
  'CV3',
  'Ureilite-an',
  'Stone-uncl',
  'L3',
  'Angrite',
  'LL6',
  'L4',
  'Aubrite',
  'Iron, IIAB',
  'Iron, IAB-sLL',
  'Iron, ungrouped',
  'CM2',
  'OC',
  'Mesosiderite-A1',
  'LL4',
  'C2-ung',
  'LL3.8',
  'Howardite',
  'Eucrite-pmict',
  'Diogenite',
  'LL3.15',
  'LL3.9',
  'Iron, IAB-MG',
  'H/L3.9',
  'Eucrite',
  'H4-an',
  'L/LL6',
  'Iron, IIIAB',
  'H/L4',
  'H4-5',
  'L3.7',
  'LL3.4',
  'Martian (chassignite)',
  'EL6',
  'H3.8',
  'H3-5',
  'H5-6',
  'Mesosiderite',
  'H5-7',
  'L3-6',
  'H4-6',
  'Ureilite',
  'Iron, IID',
  'Mesosiderite-A3/4',
  'CO3.3',
  'H3',
  'EH3/4-an',
  'Iron, IIE',
  'L/LL5',
  'H3.7',
  'CBa',
  'H4/5',
  'H3/4',
  'H?',
  'H3-6',
  'L3.4',
  'Iron, IAB-sHL',
  'L3.7-6',
  'EH7-an',
  'Iron',
  'CR2',
  'CO3.2',
  'K3',
  'L5/6',
  'CK4',
  'Iron, IIE-an',
  'L3.6',
  'LL3.2',
  'CO3.5',
  'Lodranite',
  'Mesosiderite-A3',
  'L3-4',
  'H5/6',
  'Pallasite, PMG',
  'Eucrite-cm',
  'Pallasite',
  'L5-6',
  'CO3.6',
  'Martian (nakhlite)',
  'LL3.6',
  'C3-ung',
  'H3-4',
  'CO3.4',
  'EH3',
  'Iron, IAB-ung',
  'Winonaite',
  'LL',
  'Eucrite-br',
  'Iron, IIF',
  'R3.8-6',
  'L4-6',
  'EH5',
  'LL3.00',
  'H3.4',
  'Martian (shergottite)',
  'Achondrite-ung',
  'LL3.3',
  'C',
  'H/L3.6'
];

export const FILTER_CONDITIONS = [
  { key: 'eq', value: 'eq', text: 'Equal' },
  { key: 'neq', value: 'neq', text: 'Not Equal' },
  { key: 'lte', value: 'lte', text: 'Lesser Than or Equal' },
  { key: 'gte', value: 'gte', text: 'Greater Than or Equal' },
  { key: 'range', value: 'range', text: 'Range' },
  { key: 'n_range', value: 'n_range', text: 'Not in Range' },
  { key: 'in', value: 'in', text: 'In' },
  { key: 'n_in', value: 'n_in', text: 'Not In' },
  { key: 'sw', value: 'sw', text: 'Starts With' },
  { key: 'ew', value: 'ew', text: 'Ends with' },
  { key: 'contains', value: 'contains', text: 'Contains' }
];

export const FILTER_FIELDS = [
  { key: 'name', value: 'name', text: 'Name' },
  { key: 'fall', value: 'fall', text: 'Fall?' },
  { key: 'mass', value: 'mass', text: 'Mass' },
  { key: 'year', value: 'year', text: 'Year' },
  { key: 'recLat', value: 'recLat', text: 'Latitude' },
  { key: 'recLong', value: 'recLong', text: 'Longitude' }
];

export const FALL_OPTIONS = [
  { key: 'fell', value: 'fell', text: 'Fell' },
  { key: 'found', value: 'found', text: 'Found' }
];

export const conditionSymbols = new Map();
conditionSymbols.set('=', 'eq');
conditionSymbols.set('eq', '=');

conditionSymbols.set('!=', 'neq');
conditionSymbols.set('neq', '!=');

conditionSymbols.set('<=', 'lte');
conditionSymbols.set('lte', '<=');

conditionSymbols.set('>=', 'gte');
conditionSymbols.set('gte', '>=');

conditionSymbols.set('<=>', 'range');
conditionSymbols.set('range', '<=>');

conditionSymbols.set('!(<=>)', 'n_range');
conditionSymbols.set('n_range', '!(<=>)');

conditionSymbols.set('IN', 'in');
conditionSymbols.set('in', 'IN');

conditionSymbols.set('NOT IN', 'n_in');
conditionSymbols.set('n_in', 'NOT IN');

conditionSymbols.set('a%', 'sw');
conditionSymbols.set('sw', 'a%');

conditionSymbols.set('%a', 'ew');
conditionSymbols.set('ew', '%a');

conditionSymbols.set('%a%', 'contains');
conditionSymbols.set('contains', '%a%');
