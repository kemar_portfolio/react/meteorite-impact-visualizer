// for unit testing purposes
export const MOCK_DATA = [
  {
    name: 'Aachen',
    id: '1',
    nametype: 'Valid',
    reccLass: 'L5',
    mass: '21',
    fall: 'Fell',
    year: '1880-01-01T00:00:00.000',
    recLat: '50.775000',
    recLong: '6.083330',
    geolocation: { type: 'Point', coordinates: [6.08333, 50.775] }
  },
  {
    name: 'Aarhus',
    id: '2',
    nameType: 'Valid',
    recClass: 'H6',
    mass: '720',
    fall: 'Fell',
    year: '1951-01-01T00:00:00.000',
    recLat: '56.183330',
    recLong: '10.233330',
    geolocation: { type: 'Point', coordinates: [10.23333, 56.18333] }
  }
];
