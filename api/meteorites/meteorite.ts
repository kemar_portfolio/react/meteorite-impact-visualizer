import axios from 'axios';
import moment from 'moment';
import has from 'lodash/has';
// import fs from 'fs';
import fs from 'fs-extra';
import { API_ENDPOINT } from '../../src/constants';
export const LOCAL_FILE_PATH = './meteorites.json';

export enum nameType {
  VALID = 'Valid',
  INVALID = 'InValid'
}

export enum foundOrFell {
  FELL = 'Fell',
  FOUND = 'Found'
}

export interface IMeteorite {
  id: number;
  name: string;
  nametype: nameType;
  recclass: string;
  mass: string;
  fall: foundOrFell;
  year: string;
  reclat: string;
  reclong: string;
}

export class Meteorite {
  id: number;
  name: string;
  nameType: nameType;
  recClass: String;
  mass: number;
  fall: foundOrFell;
  year: number;
  recLat: number;
  recLong: number;

  constructor(meteorite: IMeteorite) {
    if (!meteorite) {
      return;
    }
    this.id = meteorite.id;
    this.name = meteorite.name;
    this.nameType = meteorite.nametype;
    this.recClass = meteorite.recclass;
    this.mass = parseFloat(meteorite.mass);
    this.fall = meteorite.fall;
    this.year = moment(meteorite.year).year();
    this.recLat = parseFloat(meteorite.reclat) || 0;
    this.recLong = parseFloat(meteorite.reclong) || 0;
  }
}

export async function requestNasaData(): Promise<Meteorite[]> {
  let meteorites: Meteorite[] = [];
  try {
    if (fs.existsSync(LOCAL_FILE_PATH)) {
      const data = await fs.readJson(LOCAL_FILE_PATH);
      meteorites = data.map((d: IMeteorite) => new Meteorite(d));
      // await fs.remove(LOCAL_FILE_PATH);
    } else {
      const { data } = await axios.get(API_ENDPOINT.nasa);
      await fs.writeJson(LOCAL_FILE_PATH, data);
      meteorites = data.map((d: IMeteorite) => new Meteorite(d));
    }

    return Promise.resolve(meteorites);
  } catch (e) {
    return Promise.reject(e);
  }
}
