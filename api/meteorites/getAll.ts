import { Meteorite, requestNasaData } from './meteorite';
import { NowRequest, NowResponse } from '@now/node';

export default async (req: NowRequest, res: NowResponse) => {
  requestNasaData()
    .then((meteorites: Meteorite[]) => {
      res.send(meteorites);
    })
    .catch((e: any) => {
      res.send(e);
    });
};
