import isEmpty from 'lodash/isEmpty';
import { Meteorite, requestNasaData } from './meteorite';
const searchJs = require('searchjs');

export enum filterCondition {
  EQUAL = 'eq',
  NOT_EQUAL = 'neq',
  STARTS_WITH = 'sw',
  ENDS_WITH = 'ew',
  IN = 'in',
  NOT_IN = 'n_in',
  CONTAINS = 'contains',
  RANGE = 'range'
}

export interface IFilter {
  field: string;
  value: string | number | number[];
  condition: filterCondition;
}

export enum searchableFields {
  REC_CLASS = 'recClass',
  MASS = 'mass',
  FALL = 'fall',
  YEAR = 'year',
  REC_LAT = 'recLat',
  REC_LONG = 'recLong'
}
export class Query {
  filters?: IFilter[];
  constructor(filters: IFilter[]) {
    this.filters = filters || [];
  }

  async validate(): Promise<boolean> {
    const noConditions = isEmpty(this.filters);
    if (noConditions) {
      return Promise.resolve(false);
    } else {
      return Promise.resolve(true);
    }
  }

  parseYear = year => {
    if (Array.isArray(year)) {
      return year.map(y => parseInt(y, 10));
    } else {
      return parseInt(year, 10);
    }
  };

  async search(filters: IFilter[], meteorites: any[]) {
    let data = meteorites;
    if (isEmpty(filters)) {
      return Promise.resolve(data);
    }
    filters.forEach((filter: IFilter) => {
      let { field, condition, value } = filter;
      if (field === 'year') {
        value = this.parseYear(value);
      }
      switch (condition) {
        case filterCondition.EQUAL:
        case filterCondition.NOT_EQUAL:
        case filterCondition.IN:
        case filterCondition.NOT_IN:
          data = searchJs.matchArray(data, { [field]: value });
          break;
        case filterCondition.STARTS_WITH:
          data = searchJs.matchArray(data, { [field]: value, _start: true });
          break;
        case filterCondition.ENDS_WITH:
          data = searchJs.matchArray(data, { [field]: value, _end: true });
          break;
        case filterCondition.CONTAINS:
          data = searchJs.matchArray(data, { [field]: [value], _text: true });
          break;
        case filterCondition.RANGE: {
          const [from, to] = value as number[];
          data = searchJs.matchArray(data, {
            [field]: { from, to },
            _text: true
          });

          break;
        }
        default:
          data = searchJs.matchArray(data, { [field]: { [condition]: value } });
          break;
      }
    });

    return Promise.resolve(data);
  }

  async results(): Promise<Meteorite[]> {
    const { filters } = this;
    let meteorites = await requestNasaData();
    const queryResponse = await this.search(filters, meteorites);

    return Promise.resolve(queryResponse);
  }
}
