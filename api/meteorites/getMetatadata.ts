import { Meteorite, requestNasaData } from './meteorite';
import { NowRequest, NowResponse } from '@now/node';
import minBy from 'lodash/minBy';

const getMinYears = (meteorites: Meteorite[]) =>
  meteorites.map(m => m.year).sort()[0];

export default async (req: NowRequest, res: NowResponse) => {
  const {
    query: { type }
  } = req;

  requestNasaData()
    .then((meteorites: Meteorite[]) => {
      if (type === 'minYear') {
        const minYear = getMinYears(meteorites);
        res.send(minYear);
      }
    })
    .catch((e: any) => {
      res.send(e);
    });
};
