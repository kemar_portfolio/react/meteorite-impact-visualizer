import { Query, IFilter } from './query';
import { NowRequest, NowResponse } from '@now/node';
import isEmpty from 'lodash/isEmpty';

export default async (req: NowRequest, res: NowResponse) => {
  const { body } = req;
  const query = new Query(body as IFilter[]);
  const meteorites = await query.results();

  res.status(200).json({
    count: meteorites.length,
    meteorites
  });
};
